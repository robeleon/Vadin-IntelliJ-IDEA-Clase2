package com.jetbrains;

import javax.servlet.annotation.WebServlet;

import com.vaadin.annotations.Theme;
import com.vaadin.annotations.VaadinServletConfiguration;
import com.vaadin.server.VaadinRequest;
import com.vaadin.server.VaadinServlet;
import com.vaadin.shared.ui.MarginInfo;
import com.vaadin.ui.*;

/**
 * This UI is the application entry point. A UI may either represent a browser window 
 * (or tab) or some part of a html page where a Vaadin application is embedded.
 * <p>
 * The UI is initialized using {@link #init(VaadinRequest)}. This method is intended to be 
 * overridden to add component to the user interface and initialize non-component functionality.
 */
@Theme("mytheme")
public class MyUI extends UI {

    @Override
    protected void init(VaadinRequest vaadinRequest) {
      /* final HorizontalLayout layout = new HorizontalLayout();
        layout.setMargin(new MarginInfo(true,false,false,true
        ));
        final TextField name = new TextField();
        name.setCaption("Ingresa nombre:");
        final TextField apellido = new TextField();
        apellido.setCaption("Ingresa apellido:");
        Button button = new Button("Agregar");
        final HorizontalLayout layoutBtn = new HorizontalLayout();
        layoutBtn.setMargin(new MarginInfo(false,false,true,false));
        button.addClickListener( e -> {
           // layout.addComponent(new Label("gracias " + name.getValue() +apellido.getValue()
                  //  + ", esto es un ejercicio del curso Java Avanzado"));
        });
       //layoutBtn.addComponents(button);
        layout.addComponents(name, apellido,button);
       // layout.addComponents(layoutBtn);
        setContent(layout);
       // setContent(layoutBtn);*/
        final HorizontalLayout hfac=new HorizontalLayout();
        final VerticalLayout vLay=new VerticalLayout();
        hfac.setMargin(new MarginInfo(true,true,true,true));
        final TextField tFac=new TextField("No. Factura");
        final TextField tNom=new TextField("Nombre");
        final TextField monto = new TextField("Monto");
        final Button agregar=new Button( "Agregar");
        agregar.addClickListener( e -> {
             vLay.addComponent(new Label("No. Factura: " + tFac.getValue() +" Nombre: "+tNom.getValue()+" Monto: "+monto.getValue()));
        });
        hfac.setSpacing(true);
        hfac.addComponents(tFac,tNom,monto,agregar);

        hfac.addComponent(vLay);
       // hfac.setHeight("100%");
        setContent(hfac);

    }

    @WebServlet(urlPatterns = "/*", name = "MyUIServlet", asyncSupported = true)
    @VaadinServletConfiguration(ui = MyUI.class, productionMode = false)
    public static class MyUIServlet extends VaadinServlet {
    }
}
